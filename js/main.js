
/************ ************ EQHEIGHT ************ ************/

$(function() {
  fadeHeader();

    $('.eqHeight').matchHeight();
	magicBackstretch();

	$('.moreInfoTrigger').click(function() {
		$( '.moreInfoTrigger' ).removeClass('isOpen');
		$( this ).toggleClass('isOpen');
	});


	$('.header__burger').click(function() {
		$( this ).toggleClass('isOpen');
		$( '.header__navi' ).toggleClass('isOpen');
	});

  // navi
  $('.header__navi').click(function( event ) {
    $( this ).removeClass('isOpen');
    $( '.header__burger' ).removeClass('isOpen');
  });


  $('a[href*="#"]').on('click',function(e) {
   e.preventDefault();
   var target = this.hash;
   var offset = 100;
   var $target = $(target);
   var scrollPosition = $target.offset().top - offset;
   $('html, body').stop().animate({
    'scrollTop': scrollPosition
   }, 900, 'swing', function () {
    // window.location.hash = target;
   });
  });

  $(".galerySlider--start").slick({
    autoplay: true,
    autoplaySpeed: 2000,
    centerMode: false,
    dots: false,
    arrows: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    infinity: true,
    responsive: [
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ]
  });

  $(".galerySlider--footer").slick({
    autoplay: true,
    autoplaySpeed: 2000,
    centerMode: false,
    dots: false,
    arrows: false,
    slidesToShow: 5,
    slidesToScroll: 1,
    infinity: true,
    responsive: [
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinity: true
        }
      },
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinity: true
        }
      }
    ]
  });

  $(".recipeSlider").slick({
    autoplay: false,
    autoplaySpeed: 2000,
    centerMode: false,
    dots: false,
    arrows: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    infinity: false,
    responsive: [
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true
        }
      },
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ]
  });




});

/************ ************ EQHEIGHT ************ ************/

/************ ************ BACKSTRETCH ************ ************/

function magicBackstretch()
{

	var alignX = "";
	var alignY = "";
	var img = 0;

	$(".backstretchImg").each( function()
	{
		try
		{
			img = $(this).find("img");
			alignX = $(this).data("alignx");
			alignY = $(this).data("aligny");

			if( typeof alignX != "undefined" && typeof alignY != "undefined" ) {
				$(this).backstretch( img.attr("src"), { alignX: alignX, alignY: alignY });
				// document.title = 'both';
			}
			else if( typeof alignX != "undefined") {
				$(this).backstretch( img.attr("src"), { alignX: alignX });
				// document.title = 'alignX';
			}
			else if( typeof alignY != "undefined") {
				$(this).backstretch( img.attr("src"), { alignY: alignY });
				// document.title = 'alignY';
			} else {
				$(this).backstretch( img.attr("src") );
			}
			$(this).find('img').attr("alt", img.attr("alt")  );

			img.remove();
		}
		catch( err )
		{
			document.title = err.message;
		}
	});
}
/************ ************ BACKSTRETCH ************ ************/



// scroll
$(window).scroll(function (event) {
    fadeHeader();
});

// fadeHeader
function fadeHeader() {
    var scrollMax = 10;
    var scrollTop = $(window).scrollTop();

    if (scrollTop >= scrollMax) {
        $('.header').addClass('isScrolling');
    } else {
        $('.header').removeClass('isScrolling');
    }
}



// showTeaserRecipe
$('.teaser button').click(function( event ) {
  event.preventDefault();
  var parents = $( this ).parents('.teaser');

  parents.toggleClass('isOpen');
  // $('.recipeSlider').slick('refresh');
});

// showTeaserRecipe
$('.contactForm__button--dropdown').click(function() {
  var parents = $( this ).parent();

  $( this ).toggleClass('isOpen');
  parents.find('.contactForm__content').toggleClass('isOpen');
});


/************ ************ TOGGLE ************ ************/


function openFooterContent( content ) {
  $( '.footerContent' ).show();
  $( '.footerContent__inner' ).hide();
  $( '#' + content ).show();


  var offset = 100;
  var scrollPosition = $( '#' + content ).offset().top - offset;
	$('html, body').animate({scrollTop: scrollPosition}, 1000);
}
$('.footerContent__close').click(function() {
  $('.footerContent').slideUp();
});


$('.contactForm__reset').click(function() {
  $( this ).parents('form').find('input').val("");
});
